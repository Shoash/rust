# Copyright 2017 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cargo [ rust_minimum_version=1.70.0 ]
require bash-completion zsh-completion

SUMMARY="A cat(1) clone with syntax highlighting and Git integration"

LICENCES="|| ( Apache-2.0 MIT )"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8"

MYOPTIONS="
    fish-completion [[ description = [ Install completion files for the fish shell ] ]]
"

DEPENDENCIES=""

BASH_COMPLETIONS=(
    "assets/completions/${PN}.bash _${PN}"
)

ZSH_COMPLETIONS=(
    "assets/completions/${PN}.zsh _${PN}"
)

src_compile() {
    BAT_ASSETS_GEN_DIR="${WORK}" \
        cargo_src_compile
}

src_install() {
    cargo_src_install
    bash-completion_src_install
    zsh-completion_src_install

    if optionq fish-completion; then
        insinto /usr/share/fish/vendor_completions.d/
        newins "assets/completions/${PN}.fish" "${PN}.fish"
    fi

    doman assets/manual/${PN}.1
}

pkg_postinst() {
    einfo "bat 0.13.0 changes config files syntax"
    einfo "Consult 'man bat' to update any --map-syntax settings"
}

